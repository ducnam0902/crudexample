package api.demo.database;

import api.demo.models.Product;
import api.demo.repositories.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.crypto.Data;

@Configuration
public class Database {

    //logger
    private static final Logger logger = LoggerFactory.getLogger(Database.class);

    @Bean
    CommandLineRunner initDatabase(ProductRepository productRepository){

        return new CommandLineRunner() {
            @Override
            public void run(String... args) throws Exception {
                Product productA = new Product(1L, "MacBook Pro 16", 2020, 2400.0, "");
                Product productB = new Product(2L, "iPad Air Green", 2021, 599.0, "");
                logger.info("insert data: "+ productRepository.save(productA));
                logger.info("insert data: "+ productRepository.save(productB));
            }
        };
    }
}
