package api.demo.Controller;

import api.demo.models.Product;
import api.demo.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path ="/api/v1/Products")
public class ProductController {

    //DI Dependency Injection
    @Autowired
    private ProductRepository repository;

    @GetMapping("/getAllProduct")
    List<Product> getAllProduct(){

        return repository.findAll();
    }
}
